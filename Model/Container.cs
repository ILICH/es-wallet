﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using test07events;

namespace Model
{
    public class Container : UnityContainer
    {
        public Container()
        {
            this.RegisterType<IEventStoreRepository, EventStoreRepository>();
            this.RegisterType<IDomainRepository, DomainRepository>();
        }
    }
}
