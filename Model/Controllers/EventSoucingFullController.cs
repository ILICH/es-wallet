using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace test07events
{
    public class EventSoucingFullController : EventSourcingLiteController
    {
        public EventSoucingFullController(IDomainRepository repository) : base(repository) { }

        public override void PlaceBet(PlaceBetRequest request)
        {
            var playerId = GetPlayerId(request.Token);
            var player = _repository.GetPlayer(playerId);

            player.PlaceBet(request.TransactionId, request.Amount);

            _repository.Store(player, forceSnapshot: false, maxEventsBeforeSnapshot: 200);

            Interlocked.Increment(ref Stats.Bets);
        }

        public override void WinBet(WonBetRequest request)
        {
            var playerId = GetPlayerId(request.Token);
            var player = _repository.GetPlayer(playerId);

            player.WinBet(request.TransactionId, request.Amount);

            _repository.Store(player, forceSnapshot: false, maxEventsBeforeSnapshot: 200);
        }
    }
}
