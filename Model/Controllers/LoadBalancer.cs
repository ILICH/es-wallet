using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading;

namespace test07events
{
    public class LoadBalancer : IController
    {
        readonly Func<IController>[] _controllerFactories;

        public LoadBalancer(Func<IController>[] controllerFactories)
        {
           _controllerFactories = controllerFactories;
        }
        
        #region IController implementation

        public AuthenticateResponse Authenticate(AuthenticateRequest request)
        {
            return GetPlayerController(request.PlayerId).Authenticate(request);
        }
        public void PlaceBet(PlaceBetRequest request)
        {
            var controller = GetPlayerController(Guid.Parse(request.Token));
            controller.PlaceBet(request);
        }
        public void WinBet(WonBetRequest request)
        {
            var controller = GetPlayerController(Guid.Parse(request.Token));
            controller.WinBet(request);
        }

        #endregion

        IController GetPlayerController(Guid playerId)
        {
            var index = BitConverter.ToInt64(playerId.ToByteArray(), 0) % 2;
            return _controllerFactories[index]();
        }

        IController GetRandomController()
        {
            var rand = new Random();
            var index = rand.Next() % _controllerFactories.Length;
            return _controllerFactories[index]();
        }
    }
}
