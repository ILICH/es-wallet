using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace test07events
{
    public class EventSourcingLiteController : IController
    {
        protected readonly IDomainRepository _repository;

        public EventSourcingLiteController(IDomainRepository repository)
        {
            _repository = repository;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest request)
        {
            var player = _repository.GetPlayerOrNull(request.PlayerId);
            if (player == null)
            {
                player = new Player(request.PlayerId, request.PlayerName, request.Balance);
            }
            else
            {
                player.UpdateProfile(request.PlayerName, request.Balance);
            }

            _repository.Store(player, forceSnapshot: true);

            Interlocked.Increment(ref Stats.Players);

            return new AuthenticateResponse
            {
                //for simplicity we'll use PlayerId instead of real token
                Token = request.PlayerId.ToString()
            };
        }

        public virtual void PlaceBet(PlaceBetRequest request)
        {
            var playerId = GetPlayerId(request.Token);
            var player = _repository.GetPlayer(playerId);

            player.PlaceBet(request.TransactionId, request.Amount);

            _repository.Store(player, forceSnapshot: true);

            Interlocked.Increment(ref Stats.Bets);
        }

        public virtual void WinBet(WonBetRequest request)
        {
            var playerId = GetPlayerId(request.Token);
            var player = _repository.GetPlayer(playerId);

            player.WinBet(request.TransactionId, request.Amount);

            _repository.Store(player, forceSnapshot: true);
        }


        protected Guid GetPlayerId(string token)
        {
            //simplified PlayerId extraction
            return Guid.Parse(token);
        }
    }

    public class AuthenticateRequest
    {
        public Guid     PlayerId;
        public string   PlayerName;
        public decimal  Balance;
    }

    public class AuthenticateResponse
    {
        public string   Token { get; set; }
    }

    public class PlaceBetRequest
    {
        public string   Token { get; set; }
        public string   TransactionId { get; set; }
        public decimal  Amount { get; set; }
    }

    public class WonBetRequest
    {
        public string   Token { get; set; }
        public string   TransactionId { get; set; }
        public decimal  Amount { get; set; }
    }

}
