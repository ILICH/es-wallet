using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace test07events
{
    public interface IController
    {
        AuthenticateResponse    Authenticate(AuthenticateRequest request);
        void                    PlaceBet(PlaceBetRequest request);
        void                    WinBet(WonBetRequest request);
    }

}
