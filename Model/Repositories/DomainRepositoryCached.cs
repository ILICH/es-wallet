using System;
using System.Collections.Concurrent;
using System.Threading;

namespace test07events
{
    public class DomainRepositoryCached : IDomainRepository
    {
        private readonly Func<DomainRepository>                 _domainRepositoryFactory;
        private readonly ConcurrentDictionary<Guid, Player>     _players = new ConcurrentDictionary<Guid, Player>();

        public DomainRepositoryCached(Func<DomainRepository> domainRepositoryFactory)
        {
            _domainRepositoryFactory = domainRepositoryFactory;
        }

        public Player GetPlayerOrNull(Guid playerId)
        {

            if (!_players.ContainsKey(playerId))
            {
                var domainRepository = _domainRepositoryFactory();
                var player = domainRepository.GetPlayerOrNull(playerId);
                if (player == null) return null;
                _players.AddOrUpdate(playerId, player, (id, p) => player);
            }

            return _players[playerId];
        }

        public Player GetPlayer(Guid playerId)
        {
            var player = GetPlayerOrNull(playerId);
            if (player == null) throw new ApplicationException("Player not found");
            return player;
        }

        public void Store(Player player, int maxEventsBeforeSnapshot = 200, bool forceSnapshot = false)
        {

            var domainRepository = _domainRepositoryFactory();
            domainRepository.Store(player, maxEventsBeforeSnapshot, forceSnapshot);
        }

        
    }
}