using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading;

namespace test07events
{
    public class EventStoreRepositoryCached : IEventStoreRepository
    {   
        readonly ConcurrentDictionary<Guid, ConcurrentBag<IEvent>> _eventsCached = new ConcurrentDictionary<Guid, ConcurrentBag<IEvent>>();
        readonly Func<EventStoreRepository> _repositoryFactory;

        public EventStoreRepositoryCached(Func<EventStoreRepository> repositoryFactory)
        {
            this._repositoryFactory = repositoryFactory;
        }

        public IEnumerable<IEvent> GetAggregateEvents(Guid aggregateId, DateTimeOffset lastEvent)
        {
            if (_eventsCached == null || !_eventsCached.ContainsKey(aggregateId))
            {
                //first call will fetch database and we will cache all events for this aggregate since last snapshot
                var repository = _repositoryFactory();
                var aggregateEvents = repository.GetAggregateEvents(aggregateId, lastEvent).ToArray();
                _eventsCached.AddOrUpdate(
                    aggregateId, 
                    (_)     => new ConcurrentBag<IEvent>(aggregateEvents), 
                    (_, bagToUpdate)  => new ConcurrentBag<IEvent>(aggregateEvents));
                return aggregateEvents.AsQueryable();
            }

            //we will rely on load-balancer logic in order to 'stick' client's requests to 1 particular node in a cluster
            //in such way we can guarantee transactional consistency by handling it on application level
            var eventsCached = _eventsCached[aggregateId];
            return eventsCached;
        }

        public void Store(IEvent @event)
        {
            var repository = _repositoryFactory();
            
            _eventsCached.AddOrUpdate(
                    @event.AggregateId, 
                    (_)                 => new ConcurrentBag<IEvent>(new[] { @event }), 
                    (_, bagToUpdate)    => {
                        bagToUpdate.Add(@event);
                        return bagToUpdate;
            });

            repository.Store(@event);
        }
    }

}
