using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading;

namespace test07events
{
    public interface IEventStoreRepository
    {
        IEnumerable<IEvent> GetAggregateEvents(Guid aggregateId, DateTimeOffset lastEvent);
        void Store(IEvent @event);
    }

    public class EventStoreRepository : IEventStoreRepository
    {
        static readonly ConcurrentBag<IEvent> _events = new ConcurrentBag<IEvent>();

        public IEnumerable<IEvent> GetAggregateEvents(Guid aggregateId, DateTimeOffset lastEvent)
        {
            SimulateNetworkDelay();
            Interlocked.Increment(ref Stats.Reads);
            return _events.Where(x => x.AggregateId == aggregateId && x.Created > lastEvent).ToArray();
        }

        public void Store(IEvent @event)
        {
            SimulateNetworkDelay();
            _events.Add(@event);
            Interlocked.Increment(ref Stats.Writes);
        }

        void SimulateNetworkDelay()
        {
            Thread.Sleep(2);
        }
    }


}
