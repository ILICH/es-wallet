using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading;

namespace test07events
{
    public interface IDomainRepository
    {
        Player  GetPlayerOrNull(Guid playerId);
        Player  GetPlayer(Guid playerId);
        void    Store(Player player, int maxEventsBeforeSnapshot = 200, bool forceSnapshot = false);
    }

    public class DomainRepository : IDomainRepository
    {
        static readonly ConcurrentBag<PlayerData>   _players = new ConcurrentBag<PlayerData>();

        readonly IEventStoreRepository _eventStore;

        public DomainRepository(IEventStoreRepository eventStore)
        {
            _eventStore = eventStore;
        }

        public Player GetPlayerOrNull(Guid playerId)
        {
            SimulateNetworkDelay();
            var playerData = _players.SingleOrDefault(x => x.Id == playerId);
            Interlocked.Increment(ref Stats.Reads);

            if (playerData == null)
            {
                //we assume that creation of the player happens 
                //at the same time and in sync with it's first snapshot
                //meaning: no snapshot - no such player
                return null;
            }
            var aggregateEvents = _eventStore.GetAggregateEvents(playerId, playerData.LastEvent);
            return new Player(playerData, aggregateEvents);
        }

        public Player GetPlayer(Guid playerId)
        {
            var player = GetPlayerOrNull(playerId);
            if (player == null) throw new ApplicationException("Player not found");
            return player;
        }

        public void Store(Player player, int maxEventsBeforeSnapshot = 200, bool forceSnapshot = false)
        {
            SimulateNetworkDelay();
            foreach (var newEvent in player.NewEvents)
            {
                _eventStore.Store(newEvent);
            }
            player.ResetNewEvents();

            //store aggregate (snapshot) only after specific number of new events 
            if (!forceSnapshot && player.AggregatedCount < maxEventsBeforeSnapshot) return;

            var playerData = _players.SingleOrDefault(x => x.Id == player.Id);
            Interlocked.Increment(ref Stats.Reads);

            if(playerData == null)
            {
                //create new
                playerData = player.Data;
                _players.Add(playerData);
                Interlocked.Increment(ref Stats.Writes);
            }
            else
            {
                //update existing
                playerData.LastEvent = player.Data.LastEvent;
                playerData.Name = player.Data.Name;
                playerData.Wallets.Single().Balance = player.Data.Wallets.Single().Balance;
                Interlocked.Increment(ref Stats.Writes);
            }
        }

        void SimulateNetworkDelay()
        {
            Thread.Sleep(2);
        }
    }
}
