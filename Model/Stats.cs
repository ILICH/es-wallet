namespace test07events
{
    public static class Stats
    {
        public static void Reset()
        {
            Reads = 0;
            Writes = 0;
            Players = 0;
            Bets = 0;
        }

        public static int Reads;
        public static int Writes;

        public static int Players;
        public static int Bets;
    }
}