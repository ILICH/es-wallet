using System;
using System.Collections.Generic;
using System.Linq;

namespace test07events
{
    public class Player : EntityBase<PlayerData>,
        IAggregator<PlayerCreated>,
        IAggregator<PlayerUpdated>,
        IAggregator<MoneyDeposited>,
        IAggregator<BetPlaced>, 
        IAggregator<BetWon>

    {
        readonly object _syncRoot = new object();

        public Player(Guid id, string name, decimal balance) : base(id) 
        {
            ValidateName(name);
            ValidateBalance(balance);

            var @event = new PlayerCreated
            {
                Id = Guid.NewGuid().ToString(), 
                AggregateId = id, 
                PlayerName = name, 
                Wallets = new[] { new WalletData { Balance = balance } },
                Created = DateTimeOffset.Now
            };
            Aggregate(@event);

            _newEvents.Add(@event);
        }
        public void Aggregate(PlayerCreated @event)
        {
            _data.Name = @event.PlayerName;
            _data.Wallets = new[] { new WalletData { Balance = @event.Wallets.Single().Balance } };
        }

        /// <summary>
        /// this constructor is used to restore Entity from database
        /// </summary>
        public Player(PlayerData data, IEnumerable<IEvent> events = null) : base(data, events)
        {
            ValidateName(data.Name);
            ValidateBalance(data.Wallets.Single().Balance);
        }


        public PlayerUpdated    UpdateProfile(string playerName, decimal balance)
        {
            lock (_syncRoot)
            {
                ValidateName(playerName);
                ValidateBalance(balance);

                var @event = new PlayerUpdated
                {
                    Id = Guid.NewGuid().ToString(), 
                    AggregateId = Id,
                    PlayerName = playerName, 
                    Wallets = new[] { new WalletData { Balance = balance } },
                    Created = DateTimeOffset.Now                    
                };
                Aggregate(@event);

                _newEvents.Add(@event);
                return @event;
            }
        }
        public void             Aggregate(PlayerUpdated @event)
        {
            _data.Name = @event.PlayerName;
            _data.Wallets.Single().Balance = @event.Wallets.Single().Balance;
        }

        public MoneyDeposited   DepositMoney(decimal amount)
        {
            lock (_syncRoot)
            {
                ValidateAmount(amount);

                var @event = new MoneyDeposited
                {
                    Id = Guid.NewGuid().ToString(),
                    AggregateId = Id,
                    Amount = amount,
                    Created = DateTimeOffset.Now                    
                };
                Aggregate(@event);

                _newEvents.Add(@event);
                return @event;
            }
        }
        public void             Aggregate(MoneyDeposited @event)
        {
            _data.Wallets.Single().Balance += @event.Amount;
        }

        public BetPlaced        PlaceBet(string transactionId, decimal amount)
        {
            lock (_syncRoot)
            {
                ValidatePlaceBetAmount(amount);

                var @event = new BetPlaced
                { 
                    Id = transactionId, //this will validate uniquness of the transactionId when event will be stored
                    AggregateId = Id,
                    Amount = amount,
                    Created = DateTimeOffset.Now                    
                };
                Aggregate(@event);

                _newEvents.Add(@event);
                return @event;
            }
        }
        public void             Aggregate(BetPlaced @event)
        {
            _data.Wallets.Single().Balance -= @event.Amount;
        }

        public void             WinBet(string transactionId, decimal amount)
        {
            lock (_syncRoot)
            {
                ValidateAmount(amount);

                var @event = new BetWon
                {
                    Id = transactionId, //this will validate uniquness of the transactionId when event will be stored
                    AggregateId = Id,
                    Amount = amount,
                    Created = DateTimeOffset.Now                    
                };
                Aggregate(@event);

                _newEvents.Add(@event);
            }
        }
        public void             Aggregate(BetWon @event)
        {
            _data.Wallets.Single().Balance += @event.Amount;
        }

        void ValidatePlaceBetAmount(decimal amount)
        {
            ValidateAmount(amount);
            if (amount > _data.Wallets.Single().Balance) throw new ApplicationException("Amount should be less or equal to balance");
        }

        void ValidateAmount(decimal amount)
        {
            if (amount <= 0) throw new ApplicationException("Invalid amount");
        }

        void ValidateBalance(decimal balance)
        {
            if (balance < 0) throw new ApplicationException("Invalid balance");
        }

        void ValidateName(string name)
        {
            if (name.Length < 3) throw new ApplicationException("Invalid name");
        }
    }

    public class PlayerData : AggregateStateBase
    {
        public string       Name { get; set; }
        public WalletData[] Wallets { get; set; }
    }

    public class WalletData
    {
        public decimal  Balance { get; set; }
    }

    public class PlayerCreated : EventBase
    {
        public Guid         PlayerId { get { return AggregateId; } }
        public string       PlayerName { get; set; }
        public WalletData[] Wallets { get; set; }
    }

    public class PlayerUpdated : PlayerCreated { }

    public class BetPlaced : EventBase
    {
        public decimal  Amount { get; set; }
    }

    public class BetWon : EventBase
    {
        public decimal Amount { get; set; }
    }

    public class MoneyDeposited : EventBase
    {
        public decimal  Amount { get; set; }
    }
}
