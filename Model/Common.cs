using System;
using System.Collections.Generic;
using System.Linq;

namespace test07events
{
    public interface IEntity 
    {
        Guid                Id { get; }
        IAggregateSnapshot  Data { get; }
    }

    public abstract class EntityBase<TAggregateSnapshot> : IEntity where TAggregateSnapshot : IAggregateSnapshot, new()
    {
        private int                             _aggregatedCount;
        protected readonly TAggregateSnapshot   _data;
        protected readonly List<IEvent>         _newEvents = new List<IEvent>();

        private EntityBase(TAggregateSnapshot data)
        {
            _data = data;
            _aggregatedCount = 0;
        }

        /// <summary>
        /// Seed entity from the beginning of the events stream
        /// </summary>
        protected EntityBase(Guid id, IEnumerable<IEvent> events = null) 
            : this(new TAggregateSnapshot { Id = id })
        {
            if (events == null) return;
            AggregateEvents(events.Where(x => x.AggregateId == id).ToArray());
        }

        /// <summary>
        /// Seed entity from the snapshot state
        /// </summary>
        protected EntityBase(TAggregateSnapshot data, IEnumerable<IEvent> events = null) 
            : this(data)
        {
            if (events == null) return;
            AggregateEvents(events.Where(x => x.AggregateId == _data.Id && x.Created > _data.LastEvent).ToArray());
        }

        public Guid                 Id { get { return _data.Id; } }
        public TAggregateSnapshot   Data { get { return (TAggregateSnapshot)_data.Clone(); } }
        public int                  AggregatedCount { get { return _aggregatedCount; } }
        public IEnumerable<IEvent>  NewEvents { get { return _newEvents; }}

        IAggregateSnapshot          IEntity.Data { get { return Data; } }

        void AggregateEvents(IEvent[] events)
        {
            var aggregatorType = typeof(IAggregator<>);
            foreach(var @event in events)
            {
                //this part is subject to further optimizations, because reflection tends to be slow
                var concreteAggregatorType = aggregatorType.MakeGenericType(new[]{ @event.GetType() });
                var methodInfo = concreteAggregatorType.GetMethod("Aggregate");
                methodInfo.Invoke(this, new [] { @event });
                _data.LastEvent = @event.Created;
                _aggregatedCount++;
            }
        }

        internal void ResetNewEvents()
        {
            _newEvents.Clear();
        }
    }

    public interface IAggregateSnapshot : ICloneable
    {
        Guid            Id { get; set; }

        /// <summary>
        /// this identifies date and time of the last event in this snapshot
        /// </summary>
        DateTimeOffset  LastEvent { get; set; }
    }

    public abstract class AggregateStateBase : IAggregateSnapshot
    {
        public Guid             Id { get; set; }
        public DateTimeOffset   LastEvent { get; set; }

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public interface IEvent 
    {
        /// <summary>
        /// This id should be a string in order to be able to enforce invariants 
        /// in a single call if entity requires it
        /// </summary>
        string          Id { get; }
        Guid            AggregateId { get; }
        DateTimeOffset  Created { get; }
    }

    public abstract class EventBase : IEvent
    {
        /// <summary>
        /// This id should be a string in order to be able to enforce invariants 
        /// in a single call to the event store if entity requires it
        /// </summary>
        public string           Id { get; set; } 
        public Guid             AggregateId { get; set; }
        public DateTimeOffset   Created { get; set; }
    }

    public interface IAggregator<TEvent> where TEvent : IEvent
    {
        void Aggregate(TEvent @event);
    }  
}
