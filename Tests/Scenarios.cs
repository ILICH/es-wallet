﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using test07events;
using Model;

namespace Tests
{
    [TestFixture]
    public class Scenarios
    {
        private Container _container;

        [SetUp]
        public void Init()
        {
            _container = new Container();
            Stats.Reset();
        }

        [Test]
        public void Placing_bet_ES_lite()
        {
            //simple event sourcing when we're updating aggregate snapshots on each call (good-old way)
            PlaceBetScenario(controllerFactory: (_) => _container.Resolve<EventSourcingLiteController>());
        }

        [Test]
        public void Placing_bet_optimized_ES_full()
        {
            //this scenario cuts reads and writes almost in half (no caching) 
            //by updating aggregate snapshots once in 200 events only
            PlaceBetScenario(controllerFactory: (_) => _container.Resolve<EventSoucingFullController>());
        }

        [Test]
        public void Placing_bet_optimized_ES_full_cached()
        {
            //this scenario introduces caching in a load-balancer environment
            //cuts number of reads to one order of magnitude less when
            //comparing with Placing_bet_optimized_ES_full scenario

            _container.RegisterType<IEventStoreRepository, EventStoreRepositoryCached>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IDomainRepository, DomainRepositoryCached>(new ContainerControlledLifetimeManager());

            PlaceBetScenario(controllerFactory: (node) => _container.Resolve<EventSoucingFullController>(
                new ParameterOverride("eventStore", _container.Resolve<EventStoreRepositoryCached>(node)).OnType<DomainRepository>()
            ));
        }

        void PlaceBetScenario(Func<string, IController> controllerFactory)
        {
            Measure("PlaceBetScenario", () => 
            {
                Func<IController> node1ControllerFactory = () => controllerFactory("node1");
                Func<IController> node2ControllerFactory = () => controllerFactory("node2");
                var balancer = new LoadBalancer(new[] { node1ControllerFactory, node2ControllerFactory });

                Repeat(times: 50, action: () => 
                {
                    var playerId = Guid.NewGuid();
                    var authResponse = balancer.Authenticate(new AuthenticateRequest {
                        PlayerId = playerId,
                        PlayerName = "test01",
                        Balance = 100
                    });

                    Repeat(times: 10, action: () => 
                    {
                        balancer.PlaceBet(new PlaceBetRequest {
                            Token = authResponse.Token,
                            TransactionId = Guid.NewGuid().ToString(),
                            Amount = 50
                        });

                        balancer.WinBet(new WonBetRequest {
                            Token = authResponse.Token,
                            TransactionId = Guid.NewGuid().ToString(),
                            Amount = 120
                        });
                    });
                });
            });

            ShowStats();
        }


        MeasuredData Measure(string name, Action action)
        {
            var watch = Stopwatch.StartNew();
            action();
            watch.Stop();

            var data = new MeasuredData()
            {
                Name = name,
                ElapsedMilliseconds = watch.ElapsedMilliseconds
            };

            Console.WriteLine (string.Format ("scenario {0} took {1}ms", data.Name, data.ElapsedMilliseconds));

            //this is needed for Xamarin Studio only, for some reason Trace.WriteLine does not work there
            if (Environment.GetEnvironmentVariable ("SHELL") != null) 
            {
                Trace.WriteLine (string.Format ("scenario {0} took {1}ms", data.Name, data.ElapsedMilliseconds));
            }

            return data;
        }

        void Repeat(int times, Action action)
        {
            for (var i = 0; i < times; i++)
            {
                action();
            }
        }

        void ShowStats()
        {
            Trace.WriteLine(string.Format("Total reads: {0}, total writes: {1}", Stats.Reads, Stats.Writes));
            Trace.WriteLine(string.Format("Total auth players: {0}, total bets: {1}", Stats.Players, Stats.Bets));

            //this is needed for Xamarin Studio only, for some reason Trace.WriteLine does not work there
            if (Environment.GetEnvironmentVariable("SHELL") != null)
            {
                Console.WriteLine(string.Format("Total reads: {0}, total writes: {1}", Stats.Reads, Stats.Writes));
                Console.WriteLine(string.Format("Total auth players: {0}, total bets: {1}", Stats.Players, Stats.Bets));
            }
        }
    }

    class MeasuredData
    {
        public long     ElapsedMilliseconds { get; set; }
        public string   Name { get; set; }
    }
}
